# PE Landing

Wordpress plugin added WYSIWYG-editor for create landing pages in [React-ProtopiaEcosystem-web-console](https://gitlab.com/genagl/cra-template-pe).
Need [PE-Core WP-plugin](https://gitlab.com/wp37/pe-core)

# Install

1. Deploy Wordpress on hosting

2. Install [PE-Core WP-plugin](https://gitlab.com/wp37/pe-core)

3. Install PE-Landing

4. Create and deploy [React-ProtopiaEcosystem-web-console](https://gitlab.com/genagl/cra-template-pe)

5. Edit settings of it by [instuction](http://ux.protopia-home.ru/installation/)

6. Enjoy
